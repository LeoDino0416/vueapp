// const Vue = require('./vue.js');
// const moment = require('moment');

Vue.component('todo-item', {
  props: ['todo'],
  template: '<li>{{ todo.text }}</li>'
});
Vue.component('phone-li', {
  props: ['phone'],
  template: '<li>{{ phone.text }} -- score:{{ phone.score }}</li>'
});
Vue.component('date-li', {
  props: ['date'],
  template: '<li> {{ date }}</li>'
})
new Vue({
  el: '#app',
  data: {
    greeting: 'Welcome to your Vue.js app!',
    message: 'You loaded this page on ' + new Date().toLocaleString(),
    goals: [
      {id: 0, text: 'Learn JavaScript'},
      {id: 1, text: 'Learn Vue'},
      {id: 2, text: 'Build somteting awesome'}
    ],
    phoneList: [
      {id: 0, text: 'IPhone 8+', score: 9},
      {id: 1, text: 'Samsung Galaxy Note 8', score: 9},
      {id: 2, text: 'Razor Phone', score: 6},
      {id: 3, text: 'Xiaomi Mi4', score: 4},
      {id: 4, text: 'OnePLus 5', score: 6}
    ],
    newPhone: null,
    phoneScore: 0,
    newCar: null,
    carList: ['Lamborghini', 'Ferrari', 'Audi', 'Honda'],
    min: 5,
    search: '',
    dateList: new Array(),
    newDate: null,
    recentDate: null,
    actualDate: null

  },
  methods: {
    reverseMessage: function() {
      this.greeting = this.greeting.split(' ').reverse().join(' ')
    },
    addPhone: function () {
      var next = this.phoneList.length;
      var nText = this.newPhone;
      var score = this.phoneScore;
      this.phoneList.push({id: next, text: nText, score: score});
      this.newPhone = null;
    },
    removePhone() {
      alert('entra a')
    },
    addCar: function () { 
      this.carList.unshift(this.newCar);
      this.newCar = null;
    },
    addRecentDate: function () {
      if (this.dateList.length != 0) {
        var recent = this.dateList.sort();
        var rDate = recent[0];
        this.recentDate = rDate;
      } else {
        console.log('entra else');
        this.recentDate = null;
      }
    },
    addDate: function () { 
      this.dateList.push(moment(this.newDate).format('DD-MM-YYYY'));
      // console.log(moment.duration(1, 'seconds'));
      this.newDate = null;
      this.addRecentDate()
    },
    time: function () {
      this.actualDate = moment().format('dddd DD/MM/YYYY hh:mm:ss a');
    },
    test: function () {
      console.log('mounted works');
    }
  },
  computed: {
    reverseGreeting: function () { 
      return this.greeting.split(' ').reverse().join(' ')
    },
     
    pFilterByScore: function () { 
      return this.phoneList.filter((phoneList) => phoneList.score >= this.min);
    },

    searchPhone: function () { 
      return this.phoneList.filter((phoneList) => phoneList.text.includes(this.search));
     },
    
    inputDate: function () {
      return (this.newDate != null) ? moment(this.newDate).format('DD-MM-YYYY') : null;
    },
    sortDateList: function () {
      return this.dateList.sort();
    }
  },
  mounted(){
    setInterval(this.time(), 1000);
  }
})
